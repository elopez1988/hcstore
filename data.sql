-- phpMyAdmin SQL Dump
-- version 4.9.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:8889
-- Generation Time: Jun 08, 2021 at 04:56 AM
-- Server version: 5.7.26
-- PHP Version: 7.3.9
SET
  SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET
  time_zone = "+00:00";
--
  -- Database: `inertial`
  --
  -- --------------------------------------------------------
  --
  -- Table structure for table `hcstore`
  --
  CREATE TABLE `hcstore` (
    `id` bigint(20) UNSIGNED NOT NULL,
    `Numero_de_pedido` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
    `hubwoo_ecomm_deal_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
    `USER_hubwoo_user_vid` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
    `_gateway_reference` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
    `_transaction_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
    `Product_Name_main` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
    `Categoria` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
    `Item_Name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
    `_ppom_fields` text COLLATE utf8mb4_unicode_ci,
    `colegio_sobre` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
    `ciudad_colegio` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
    `SKU` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
    `attribute_donacion` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
    `Nombre_del_Difunto` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
    `_desea_una_misa` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
    `mensaje` text COLLATE utf8mb4_unicode_ci,
    `Mensaje_Personalizado` text COLLATE utf8mb4_unicode_ci,
    `mensaje1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
    `mensaje2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
    `Nombre_y_apellido_destinatario` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
    `Fono_destinatario` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
    `Direccion_destinatario` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
    `Piso_torre_otro` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
    `Comuna` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
    `Region` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
    `Articulo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
    `Cantidad` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
    `Quantity_Refund` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
    `Quantity` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
    `Coste_de_articulo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
    `Estado_del_pedido` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
    `Fecha_del_pedido` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
    `Nota_del_cliente` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
    `rut` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
    `Nombre_facturacion` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
    `Apellidos_facturacion` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
    `Empresa_facturacion` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
    `Correo_electronico_facturacion` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
    `Telefono_facturacion` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
    `Titulo_del_metodo_de_pago` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
    `Importe_total_del_pedido` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
    `affiliate_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
    `referral_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
    `Notas_del_pedido` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
    `created_at` timestamp NULL DEFAULT NULL,
    `updated_at` timestamp NULL DEFAULT NULL,
    `deleted_at` timestamp NULL DEFAULT NULL
  ) ENGINE = InnoDB DEFAULT CHARSET = utf8mb4 COLLATE = utf8mb4_unicode_ci;
--
  -- Indexes for dumped tables
  --
  --
  -- Indexes for table `hcstore`
  --
ALTER TABLE
  `hcstore`
ADD
  PRIMARY KEY (`id`);
--
  -- AUTO_INCREMENT for dumped tables
  --
  --
  -- AUTO_INCREMENT for table `hcstore`
  --
ALTER TABLE
  `hcstore`
MODIFY
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;