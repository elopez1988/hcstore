<?php
// conexión
// $mysqli = @new mysqli('locahost:8889', 'root', 'root', 'import');

/* Database connection start */
$servername = "localhost:8889";
$username = "root";
$password = "root";
$dbname = "import";

$conn = mysqli_connect($servername, $username, $password, $dbname);
if (mysqli_connect_errno()) {
    printf("Connect failed: %sn", mysqli_connect_error());
    exit();
}

$fp = fopen("data/part.csv", "r");

while (($data = fgetcsv($fp, 1000, ",")) == true) {

    $num = count($data);
    $linea++;

    $extradata = array();
    if ($data[8] != null) {
        $extradata = unserialize($data[8]);

        // 'Nombre_del_Difunto' => $row[13],
        // 'Mensaje_Personalizado' => $row[16],
        // 'Nombre_y_apellido_destinatario' => $row[19],
        // 'Fono_destinatario' => $row[20],
        // 'Direccion_destinatario' => $row[21],
        // 'Comuna' => $row[23],
        // 'Region' => $row[24],
        $data[13] = $extradata['fields']['nombre_del_difunto'] ? $extradata['fields']['nombre_del_difunto'] : '';
        $data[16] = $extradata['fields']['mensaje_personalizado'] ? $extradata['fields']['mensaje_personalizado'] : '';
        $data[19] = $extradata['fields']['nombre_y_apellido_destinatario'] ? $extradata['fields']['nombre_y_apellido_destinatario'] : '';
        $data[20] = $extradata['fields']['fono_destinatario'] ? $extradata['fields']['fono_destinatario'] : '';
        $data[21] = $extradata['fields']['direcci_n_destinatario'] ? $extradata['fields']['direcci_n_destinatario'] : '';
        $data[23] = $extradata['fields']['comuna'] ? $extradata['fields']['comuna'] : '';
        $data[24] = $extradata['fields']['regi_n'] ? $extradata['fields']['regi_n'] : '';
    }

    $q = "INSERT INTO hcstore (
                `Numero_de_pedido`, `hubwoo_ecomm_deal_id`, `USER_hubwoo_user_vid`, `_gateway_reference`, `_transaction_id`, `Product_Name_main`, `Categoria`, `Item_Name`, `_ppom_fields`, `colegio_sobre`, `ciudad_colegio`, `SKU`, `attribute_donacion`, `Nombre_del_Difunto`, `_desea_una_misa`, `mensaje`, `Mensaje_Personalizado`, `mensaje1`, `mensaje2`, `Nombre_y_apellido_destinatario`, `Fono_destinatario`, `Direccion_destinatario`, `Piso_torre_otro`, `Comuna`, `Region`, `Articulo`, `Cantidad`, `Quantity_Refund`, `Quantity`, `Coste_de_articulo`, `Estado_del_pedido`, `Fecha_del_pedido`, `Nota_del_cliente`, `rut`, `Nombre_facturacion`, `Apellidos_facturacion`, `Empresa_facturacion`, `Correo_electronico_facturacion`, `Telefono_facturacion`, `Titulo_del_metodo_de_pago`, `Importe_total_del_pedido`, `affiliate_id`, `referral_id`, `Notas_del_pedido`
            ) VALUES (
            '$data[0]',
            '$data[1]',
            '$data[2]',
            '$data[3]',
            '$data[4]',
            '$data[5]',
            '$data[6]',
            '$data[7]',
            '$data[8]',
            '$data[9]',
            '$data[10]',
            '$data[11]',
            '$data[12]',
            '$data[13]',
            '$data[14]',
            '$data[15]',
            '$data[16]',
            '$data[17]',
            '$data[18]',
            '$data[19]',
            '$data[20]',
            '$data[21]',
            '$data[22]',
            '$data[23]',
            '$data[24]',
            '$data[25]',
            '$data[26]',
            '$data[27]',
            '$data[28]',
            '$data[29]',
            '$data[30]',
            '$data[31]',
            '$data[32]',
            '$data[33]',
            '$data[34]',
            '$data[35]',
            '$data[36]',
            '$data[37]',
            '$data[38]',
            '$data[39]',
            '$data[40]',
            '$data[41]',
            '$data[42]',
            '$data[43]'
        )";
    echo $q;
    $resultset = mysqli_query($conn, $q) or die("database error:" . mysqli_error($conn));

    echo "exito: " . $linea . "<br/>";
}

fclose($fp);
